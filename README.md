English:

 

This Plugin has one single use:

It makes you able to change between Spectator and Survival mode easily.

This plugin should be used to spectate other Players by the staff.

 

Commands:

/spec -- Change between Spectator and Survivalmode

/hspec -- Show a help Page ingame

 

Permissions:

spec.spec -- gives permission to /spec and /hspec

 

 

German:

 

Dieses Plugin hat lediglich einen Nutzen.

Das Wechseln zwischen dem Spectator- und dem Überlebensmodus.

Dieses Plugin dient für das Serverteam zum überwachen anderer spieler ohne jedem Teammitglied direkt Operator Rechte zu geben.

 

Befehle:

/spec -- Wechsle zwischen den beiden Modi.

/hspec -- Erhalte ingame eine Hilfe Seite.

 

Berechtigungen:

spec.spec -- Erlaubt das Benutzen beider befehle.